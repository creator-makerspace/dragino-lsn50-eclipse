################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/at.c \
../src/bsp.c \
../src/command.c \
../src/debug.c \
../src/hw_gpio.c \
../src/hw_rtc.c \
../src/hw_spi.c \
../src/lora.c \
../src/main.c \
../src/mlm32l0xx_hal_msp.c \
../src/mlm32l0xx_hw.c \
../src/mlm32l0xx_it.c \
../src/stm32l0xx_hal_msp.c \
../src/stm32l0xx_hw.c \
../src/stm32l0xx_it.c \
../src/stm32l1xx_hal_msp.c \
../src/stm32l1xx_hw.c \
../src/stm32l1xx_it.c \
../src/stm32l4xx_hal_msp.c \
../src/stm32l4xx_hw.c \
../src/stm32l4xx_it.c \
../src/test_rf.c \
../src/tiny_sscanf.c \
../src/tiny_vsnprintf.c \
../src/vcom.c 

OBJS += \
./src/at.o \
./src/bsp.o \
./src/command.o \
./src/debug.o \
./src/hw_gpio.o \
./src/hw_rtc.o \
./src/hw_spi.o \
./src/lora.o \
./src/main.o \
./src/mlm32l0xx_hal_msp.o \
./src/mlm32l0xx_hw.o \
./src/mlm32l0xx_it.o \
./src/stm32l0xx_hal_msp.o \
./src/stm32l0xx_hw.o \
./src/stm32l0xx_it.o \
./src/stm32l1xx_hal_msp.o \
./src/stm32l1xx_hw.o \
./src/stm32l1xx_it.o \
./src/stm32l4xx_hal_msp.o \
./src/stm32l4xx_hw.o \
./src/stm32l4xx_it.o \
./src/test_rf.o \
./src/tiny_sscanf.o \
./src/tiny_vsnprintf.o \
./src/vcom.o 

C_DEPS += \
./src/at.d \
./src/bsp.d \
./src/command.d \
./src/debug.d \
./src/hw_gpio.d \
./src/hw_rtc.d \
./src/hw_spi.d \
./src/lora.d \
./src/main.d \
./src/mlm32l0xx_hal_msp.d \
./src/mlm32l0xx_hw.d \
./src/mlm32l0xx_it.d \
./src/stm32l0xx_hal_msp.d \
./src/stm32l0xx_hw.d \
./src/stm32l0xx_it.d \
./src/stm32l1xx_hal_msp.d \
./src/stm32l1xx_hw.d \
./src/stm32l1xx_it.d \
./src/stm32l4xx_hal_msp.d \
./src/stm32l4xx_hw.d \
./src/stm32l4xx_it.d \
./src/test_rf.d \
./src/tiny_sscanf.d \
./src/tiny_vsnprintf.d \
./src/vcom.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"/home/ekarlso/eclipse-workspace/lsn50/Drivers/CMSIS/Include" -I"/home/ekarlso/eclipse-workspace/lsn50/Drivers/CMSIS/Device/ST/STM32L0xx/Include" -I"/home/ekarlso/eclipse-workspace/lsn50/Drivers/STM32L0xx_HAL_Driver/Inc" -I"/home/ekarlso/eclipse-workspace/lsn50/Drivers/BSP/STM32L0xx_Nucleo" -I"/home/ekarlso/eclipse-workspace/lsn50/Lora/Utilities" -I"/home/ekarlso/eclipse-workspace/lsn50/Lora/Crypto" -I"/home/ekarlso/eclipse-workspace/lsn50/Lora/Phy" -I"/home/ekarlso/eclipse-workspace/lsn50/Lora/Core" -I"/home/ekarlso/eclipse-workspace/lsn50/Lora/Conf" -I"/home/ekarlso/eclipse-workspace/lsn50/Lora/Mac" -I"/home/ekarlso/eclipse-workspace/lsn50/Drivers/BSP/Components/ds18b20" -I"/home/ekarlso/eclipse-workspace/lsn50/Drivers/BSP/Components/oil_float" -I"/home/ekarlso/eclipse-workspace/lsn50/inc" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


